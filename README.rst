polarion-tools-common
=====================

.. image:: https://gitlab.com/mkourim/polarion-tools-common/badges/master/pipeline.svg
    :target: https://gitlab.com/mkourim/polarion-tools-common/commits/master
    :alt: Pipeline status

.. image:: https://gitlab.com/mkourim/polarion-tools-common/badges/master/coverage.svg
    :target: https://gitlab.com/mkourim/polarion-tools-common/commits/master
    :alt: Coverage report

.. image:: https://img.shields.io/pypi/v/polarion-tools-common.svg
    :target: https://pypi.python.org/pypi/polarion-tools-common
    :alt: Version

.. image:: https://img.shields.io/pypi/pyversions/polarion-tools-common.svg
    :target: https://pypi.python.org/pypi/polarion-tools-common
    :alt: Supported Python versions

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
    :target: https://github.com/ambv/black
    :alt: Code style: black

Description
-----------
Common utilities used by Polarion tools.

Install
-------
To install the package to your virtualenv, run

.. code-block::

    pip install polarion-tools-common

or install it from cloned directory

.. code-block::

    pip install -e .

Package on PyPI <https://pypi.python.org/pypi/polarion-tools-common>
